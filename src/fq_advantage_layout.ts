/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

// tslint:disable-next-line
const readline = require('readline');

import * as fs from 'fs';
import { Config, LayoutFileType, parse_args, show_help } from './config';
// @ts-ignore
import { qwerty } from './default_keyboard_description_qwerty';
import { determine_layout_file_type } from './determine_layout_file_type';
import { generate_fq_layout } from './generate_fq_layout';
import { generate_kinesis_layout } from './generate_kinesis_layout';
import { KeyboardDescription } from './keyboard_description';
import { load_fq_layout_file } from './load_fq_layout_file';
import { load_kinesis_layout_file } from './load_kinesis_layout_file';
import { UnreachableCaseError } from './unreachable';

//- validations in keyboard descriptions

function main() {
    // const desc = new KeyboardDescription(qwerty);
    // tests(desc, default_config);

    const config = parse_args();

    if (config === undefined) {
        return;
    }

    if (config.sample !== undefined) {
        output_sample(config);

        return;
    }

    const input = load_input(config);

    if (input === undefined || input.then === undefined) {
        show_help();
        process.exit(-1);
    } else {
        input
            .then((input_lines: string[]) => {
                const desc: KeyboardDescription = parse_description(input_lines, config);
                const output: string[] = generate_layout_output(desc, config);

                output_layout(output, config);
            })
            // tslint:disable-next-line:no-any
            .catch((_e: any) => show_help());
    }
}

function output_layout(lines: string[], config: Config): void {
    if (config.outfile === undefined || config.outfile === '') {
        for (const line of lines) {
            console.log(line);
        }
    } else {
        fs.writeFileSync(config.outfile, lines.join('\n'));
    }
}

function generate_layout_output(desc: KeyboardDescription, config: Config): string[] {
    let output_format: LayoutFileType = 'fq';

    if (config.output_format === 'infer') {
        switch (config.input_format) {
            case 'infer':
            //fallthrough
            case 'fq':
                output_format = 'kinesis';
                break;
            case 'kinesis':
                output_format = 'fq';
                break;

            default:
                throw new UnreachableCaseError(config.input_format);
        }
    } else {
        output_format = config.output_format;
    }

    if (output_format === 'fq') {
        return generate_fq_layout(desc, config);
    } else {
        return generate_kinesis_layout(desc, config);
    }
}

function parse_description(lines: string[], config: Config): KeyboardDescription {
    if (config.input_format === 'infer') {
        config.input_format = determine_layout_file_type(lines);
    }

    if (config.input_format === 'fq') {
        return load_fq_layout_file(lines, config);
    } else {
        return load_kinesis_layout_file(lines, config);
    }
}

function output_sample(config: Config): void {
    if (config.sample === 'fq') {
        const initial_desc = qwerty;
        const desc = new KeyboardDescription(initial_desc);
        const output = generate_fq_layout(desc, config);
        output_layout(output, config);

        return;
    }

    if (config.sample === 'kinesis') {
        const initial_desc = qwerty;
        const desc = new KeyboardDescription(initial_desc);
        const output = generate_kinesis_layout(desc, config);
        output_layout(output, config);

        return;
    }
}

function load_input(config: Config): Promise<string[]> {
    if (config.infile === undefined) {
        // tslint:disable:no-any no-unsafe-any
        return readline.createInterface({
            input: process.stdin,
            output: process.stdout,
            terminal: false
        });
    } else {
        // tslint:disable-next-line:typedef
        return new Promise((resolve, _reject) => {
            resolve(
                fs
                    .readFileSync(config.infile as string, { encoding: 'utf-8' })
                    .toString()
                    .split('\n')
            );
        });
    }
}

// @ts-ignore
function tests(desc: KeyboardDescription, config: Config) {
    const outk1 = generate_kinesis_layout(desc, config);
    const outf1 = generate_fq_layout(desc, config);

    const ink2 = load_kinesis_layout_file(outk1, config);
    const inf2 = load_fq_layout_file(outf1, config);

    const outk2 = generate_kinesis_layout(ink2, config);
    const outf2 = generate_fq_layout(inf2, config);

    const outk3 = generate_kinesis_layout(inf2, config);
    const outf3 = generate_fq_layout(inf2, config);

    compare_outs('1', outk1, outk2);
    compare_outs('2', outk1, outk3);
    compare_outs('3', outf1, outf2);
    compare_outs('4', outf1, outf3);
}

function compare_outs(n: string, a: string[], b: string[]): void {
    if (a.length !== b.length) {
        console.error(`length mismatch: ${n}`);

        return;
    }

    for (let i = 0; i < a.length; i = i + 1) {
        if (a[i] !== b[i]) {
            console.error(`line mismatch: ${n}: ${i} ${a[i]} / ${b[i]}`);
            break;
        }
    }
}

main();
