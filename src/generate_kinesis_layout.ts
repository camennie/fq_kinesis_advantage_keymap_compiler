/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { Config } from './config';
import { KeyDescription } from './key_description';
import { KeyboardDescription } from './keyboard_description';
import { KeyboardLayer } from './keyboard_layers_description';
import { MultiKeyMacro } from './multi_key_macro';

export function generate_kinesis_layout(keyboard: KeyboardDescription, config: Config): string[] {
    let lines: string[] = [];

    lines.push(`* Generated by fq_kineses_advantage_keymap_compiler ${config.version}`);
    lines.push('*');

    lines = lines.concat(kinesis_layout(keyboard.desc.base, 'base', config));

    lines.push('*');
    lines.push('*');
    lines.push('*');

    lines = lines.concat(kinesis_layout(keyboard.desc.keypad, 'keypad', config));

    lines.push('*');
    lines.push('*');
    lines.push(`* [multi key macros]`);
    lines.push('*');
    generate_multi_key_macro_rows(lines, keyboard.multi_key_macros);
    lines.push('*');

    return lines;
}

function kinesis_layout(layer: KeyboardLayer, layer_name: string, config: Config): string[] {
    const lines: string[] = [];

    lines.push(`* ${layer_name} left function row:`);
    lines.push('*');
    generate_layout_rows(lines, [layer.left_function_row.keys], config);
    lines.push('*');

    lines.push(`* ${layer_name} right function row:`);
    lines.push('*');
    generate_layout_rows(lines, [layer.right_function_row.keys], config);
    lines.push('*');

    lines.push(`* ${layer_name} left key well rows:`);
    lines.push('*');
    generate_layout_rows(lines, layer.left_key_well.keys, config);
    lines.push('*');

    lines.push(`* ${layer_name} right key well rows:`);
    lines.push('*');
    generate_layout_rows(lines, layer.right_key_well.keys, config);
    lines.push('*');

    lines.push(`* ${layer_name} left thumb island rows:`);
    lines.push('*');
    generate_layout_rows(lines, layer.left_thumb_island.keys, config);
    lines.push('*');

    lines.push(`* ${layer_name} right thumb island rows:`);
    lines.push('*');
    generate_layout_rows(lines, layer.right_thumb_island.keys, config);
    lines.push('*');

    lines.push(`* ${layer_name} foot petal keys:`);
    lines.push('*');
    generate_layout_rows(lines, [[layer.lp, layer.mp, layer.rp]], config);
    lines.push('*');

    return lines;
}

function generate_layout_rows(lines: string[], keys: KeyDescription[][], config: Config): void {
    for (const row of keys) {
        for (const key of row) {
            add_key_desc(lines, key, config);
        }
        lines.push('*');
    }
}

function add_key_desc(lines: string[], key: KeyDescription, config: Config): void {
    const key_default_value = key.default_value.write_kinesis(key.kinesis_location);
    const key_value = key.value.write_kinesis(key.kinesis_location);

    if (config.optimize_output && key_default_value === key_value) {
        return;
    } else {
        lines.push(key_value);
    }
}

function generate_multi_key_macro_rows(lines: string[], macros: MultiKeyMacro[]): void {
    for (const macro of macros) {
        lines.push(macro.write_kinesis());
    }
}
