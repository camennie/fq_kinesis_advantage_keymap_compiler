/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { ActionTokenModifier } from './action_tokens/action_token_modifier';
import { ActionTokenSimple } from './action_tokens/action_token_simple';
import { KeyDescription } from './key_description';
import { KeyboardLayersDescription, KeyboardLayer } from './keyboard_layers_description';
import { is_valid_action_token, is_valid_location_token } from './kinesis_layout_key_defs';

const base: KeyboardLayer = {
    left_function_row: {
        keys: [k('escape'), k('f1'), k('f2'), k('f3'), k('f4'), k('f5'), k('f6'), k('f7'), k('f8')]
    },

    left_key_well: {
        keys: [
            [k('='), k('1'), k('2'), k('3'), k('4'), k('5')],
            [k('tab'), k('q'), k('w'), k('e'), k('r'), k('t')],
            [k('caps'), k('a'), k('s'), k('d'), k('f'), k('g')],
            [m('lshift'), k('z'), k('x'), k('c'), k('v'), k('b')],
            [k('`'), k('intl-\\'), k('left'), k('right')]
        ]
    },
    left_thumb_island: {
        keys: [[m('lctrl'), m('lalt')], [k('bspace'), k('delete'), k('home')], [k('end')]]
    },

    right_function_row: {
        keys: [k('f9'), k('f10'), k('f11'), k('f12'), k('prtscr'), k('scroll'), k('pause')]
    },

    right_key_well: {
        keys: [
            [k('6'), k('7'), k('8'), k('9'), k('0'), k('hyphen')],
            [k('y'), k('u'), k('i'), k('o'), k('p'), k('\\')],
            [k('h'), k('j'), k('k'), k('l'), k(';'), k("'")],
            [k('n'), k('m'), k(','), k('.'), k('/'), m('rshift')],
            [k('up'), k('down'), k('obrack'), k('cbrack')]
        ]
    },
    right_thumb_island: {
        keys: [[m('rwin'), m('rctrl')], [k('pup'), k('enter'), k('space')], [k('pdown')]]
    },

    lp: a('lp-tab', 'tab'),
    mp: a('mp-kpshf', 'kpshift'),
    rp: a('rp-kpent', 'kpenter1')
};

const keypad: KeyboardLayer = {
    left_function_row: {
        keys: [
            kp('escape'),
            mp('lwin'),
            mp('ralt'),
            k('menu'),
            k('play'),
            k('prev'),
            k('next'),
            k('calc'),
            a('kpshft', 'kpshift')
        ]
    },

    left_key_well: {
        keys: [
            [kp('='), kp('1'), kp('2'), kp('3'), kp('4'), kp('5')],
            [kp('tab'), kp('q'), kp('w'), kp('e'), kp('r'), kp('t')],
            [kp('caps'), kp('a'), kp('s'), kp('d'), kp('f'), kp('g')],
            [mp('lshift'), kp('z'), kp('x'), kp('c'), kp('v'), kp('b')],
            [kp('`'), a('kp-insert', 'insert'), kp('left'), kp('right')]
        ]
    },
    left_thumb_island: {
        keys: [[mp('lctrl'), mp('lalt')], [kp('bspace'), kp('delete'), kp('home')], [kp('end')]]
    },

    right_function_row: {
        keys: [kp('f9'), kp('f10'), kp('f11'), kp('f12'), k('mute'), k('vol-'), k('vol+')]
    },

    right_key_well: {
        keys: [
            [kp('6'), k('numlk'), k('kp='), k('kpdiv'), k('kpmult'), kp('hyphen')],
            [kp('y'), k('kp7'), k('kp8'), k('kp9'), k('kpmin'), kp('\\')],
            [kp('h'), k('kp4'), k('kp5'), k('kp6'), k('kpplus'), kp("'")],
            [kp('n'), k('kp1'), k('kp2'), k('kp3'), k('kpenter1'), mp('rshift')],
            [kp('up'), kp('down'), k('kp.'), a('kpenter2', 'kpenter1')]
        ]
    },
    right_thumb_island: {
        keys: [[mp('rwin'), mp('rctrl')], [kp('pup'), kp('enter'), k('kp0')], [kp('pdown')]]
    },

    lp: a('kp-lp-tab', 'tab'),
    mp: a('kp-mp-kpshf', 'kpshift'),
    rp: a('kp-rp-kpent', 'kpenter1')
};

export const qwerty: KeyboardLayersDescription = {
    base,
    keypad
};

// tslint:disable-next-line:function-name
function a(l: string, v: string): KeyDescription {
    if (!is_valid_location_token(l)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown location token: ${l}`);
    }

    if (!is_valid_action_token(v)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown action token: ${v}`);
    }

    return {
        kinesis_location: l,
        default_value: new ActionTokenSimple(v),
        value: new ActionTokenSimple(v)
    };
}

// tslint:disable-next-line:function-name
function k(l: string): KeyDescription {
    if (!is_valid_location_token(l)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown location token: ${l}`);
    }

    if (!is_valid_action_token(l)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown action token: ${l}`);
    }

    return {
        kinesis_location: l,
        default_value: new ActionTokenSimple(l),
        value: new ActionTokenSimple(l)
    };
}

// tslint:disable-next-line:function-name
function kp(l: string): KeyDescription {
    if (!is_valid_location_token(`kp-${l}`)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown location token: kp-${l}`);
    }

    if (!is_valid_action_token(l)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown action token: ${l}`);
    }

    return {
        kinesis_location: `kp-${l}`,
        default_value: new ActionTokenSimple(l),
        value: new ActionTokenSimple(l)
    };
}

// tslint:disable-next-line:function-name
function m(
    v: 'lshift' | 'lctrl' | 'lalt' | 'lwin' | 'rshift' | 'rctrl' | 'ralt' | 'rwin'
): KeyDescription {
    if (!is_valid_action_token(v)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown action token: ${v}`);
    }

    if (!is_valid_location_token(v)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown location token: ${v}`);
    }

    return {
        kinesis_location: v,
        default_value: new ActionTokenModifier(v),
        value: new ActionTokenModifier(v)
    };
}

// tslint:disable-next-line:function-name
function mp(
    v: 'lshift' | 'lctrl' | 'lalt' | 'lwin' | 'rshift' | 'rctrl' | 'ralt' | 'rwin'
): KeyDescription {
    if (!is_valid_action_token(v)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown action token: ${v}`);
    }

    if (!is_valid_location_token(`kp-${v}`)) {
        // tslint:disable-next-line:no-console
        console.error(`unknown location token: kp-${v}`);
    }

    return {
        kinesis_location: `kp-${v}`,
        default_value: new ActionTokenModifier(v),
        value: new ActionTokenModifier(v)
    };
}
