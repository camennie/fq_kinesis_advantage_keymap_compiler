/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { ActionTokenKeyWithModifiers } from './action_tokens/action_token_key_with_modifiers';
import { ActionTokenMacro } from './action_tokens/action_token_macro';
import { ActionTokenModifier } from './action_tokens/action_token_modifier';
import { ActionTokenNull } from './action_tokens/action_token_null';
import { ActionTokenScancode } from './action_tokens/action_token_scancode';
import { ActionTokenShifted } from './action_tokens/action_token_shifted';
import { ActionTokenSimple } from './action_tokens/action_token_simple';
import { ActionTokenTapHold } from './action_tokens/action_token_taphold';
import { Config } from './config';
import { qwerty } from './default_keyboard_description_qwerty';
import { KeyboardDescription } from './keyboard_description';
import { is_valid_location_token } from './kinesis_layout_key_defs';
import { unwrap } from './misc';
import { MultiKeyMacro } from './multi_key_macro';

export function load_kinesis_layout_file(
    base_lines: string[],
    _config: Config
): KeyboardDescription {
    const lines = base_lines.filter(
        (l: string) => !l.trim().startsWith('*') && l.trim().length !== 0
    );

    const keyboard = new KeyboardDescription(qwerty);

    const action_token_parsers = [
        new ActionTokenKeyWithModifiers(new Set(), ''),
        new ActionTokenModifier('lshift'),
        new ActionTokenNull(),
        new ActionTokenTapHold('', '', 0),
        new ActionTokenShifted('', ''),
        new ActionTokenScancode(''),
        new ActionTokenSimple(''),
        new ActionTokenMacro(keyboard.macros, '', '')
    ];

    const multi_key_macros: MultiKeyMacro[] = keyboard.multi_key_macros;

    for (const line of lines) {
        // noinspection DuplicatedCode
        const pieces = line
            .trim()
            .split('>')
            .map((s: string) => s.trim());

        if (pieces.length !== 2) {
            console.error(`Unknown directive: ${line}`);
            continue;
        }

        const locations = parse_location_tokens(pieces[0]);

        const action_token = pieces[1];
        let parsed = false;
        for (const op of action_token_parsers) {
            const token = op.parse_kinesis(action_token);

            if (token !== undefined && locations.length > 0) {
                if (locations.length === 1) {
                    keyboard.replace_key_desc(locations[0], token);
                } else {
                    multi_key_macros.push(new MultiKeyMacro(locations, token));
                }

                parsed = true;
                break;
            }
        }

        if (!parsed) {
            console.error(`Unknown directive: ${line}`);
        }
    }

    keyboard.validate_kpshift_layers();

    return keyboard;
}

const location_regex = new RegExp(/({[^}]+})/g);
export function parse_location_tokens(raw_location: string): string[] {
    const basic_location = unwrap(raw_location);

    if (basic_location !== undefined && is_valid_location_token(basic_location)) {
        return [basic_location];
    }

    const locations: string[] = [];

    let matched = location_regex.exec(raw_location);

    if (matched === null) {
        console.error(`Unknown location directive: ${raw_location}`);

        return [];
    }

    while (matched !== null) {
        for (let i = 1; i < matched.length; i = i + 1) {
            const location = unwrap(matched[i]);

            if (location === undefined) {
                console.error(`Unknown location directive: ${matched[i]} in ${raw_location}`);
                continue;
            }

            if (!is_valid_location_token(location)) {
                console.error(`Unknown location directive: ${matched[i]} in ${raw_location}`);
                continue;
            }

            locations.push(location);
        }

        matched = location_regex.exec(raw_location);
    }

    if (locations.length === 0) {
        console.error(`Unknown location directive: ${raw_location}`);

        return [];
    }

    return locations;
}
