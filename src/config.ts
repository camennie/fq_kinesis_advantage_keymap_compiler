/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

// tslint:disable-next-line:no-require-imports
import commandLineArgs = require('command-line-args');
// tslint:disable-next-line:no-require-imports
import commandLineUsage = require('command-line-usage');

export type LayoutFileType = 'kinesis' | 'fq';

export interface Config {
    version: '1.0';
    optimize_output: boolean;
    input_format: LayoutFileType | 'infer';
    output_format: LayoutFileType | 'infer';
    infile: string | undefined;
    outfile: string | undefined;
    sample: LayoutFileType | undefined;
}

export const default_config: Config = {
    version: '1.0',
    optimize_output: true,
    input_format: 'infer',
    output_format: 'infer',
    infile: '',
    outfile: '',
    sample: undefined
};

const options_def: commandLineUsage.OptionDefinition[] = [
    {
        name: 'help',
        type: Boolean,
        description: 'Print this usage guide.'
    },

    {
        name: 'infile',
        type: String,
        defaultValue: undefined,
        typeLabel: '{underline file}',
        description: 'filename to read layout from (stdin if not specified)'
    },

    {
        name: 'outfile',
        type: String,
        defaultValue: undefined,
        typeLabel: '{underline file}',
        description: 'filename to write layout to (stdout if not specified)'
    },

    {
        name: 'informat',
        type: String,
        defaultValue: undefined,
        typeLabel: '{underline <fq|kinesis>}',
        description: 'force parsing format of input file (autodetect if not specified)'
    },

    {
        name: 'outformat',
        type: String,
        defaultValue: undefined,
        typeLabel: '{underline <fq|kinesis>}',
        description:
            'layout of the output file as either a Kinesis layout or fq layout\
 (otherwise use opposite of input format if not specified)'
    },

    {
        name: 'optimize',
        type: Boolean,
        defaultValue: false,
        description: 'if specified, exclude redundant lines in the output Kinesis layout'
    },

    {
        name: 'sample',
        type: String,
        defaultValue: undefined,
        typeLabel: '{underline <fq|kinesis>}',
        description:
            'Instead of processing an input file, write out an example layout in the\
format specified'
    }
];

export function parse_args(): Config | undefined {
    // tslint:disable:no-any no-unsafe-any
    const options: any = commandLineArgs(options_def);

    if (options.help) {
        show_help();

        return undefined;
    }

    const optimize_output: boolean = options.optimize;
    const infile: string | undefined = options.infile;
    const outfile: string | undefined = options.outfile;
    let input_format: LayoutFileType | 'infer' = 'infer';
    let output_format: LayoutFileType | 'infer' = 'infer';
    let sample: LayoutFileType | undefined;

    if (options.informat === 'fq') {
        input_format = 'fq';
    }

    if (options.informat === 'kinesis') {
        input_format = 'kinesis';
    }

    if (options.outformat === 'fq') {
        output_format = 'fq';
    }

    if (options.outformat === 'kinesis') {
        output_format = 'kinesis';
    }

    if (options.sample === 'fq') {
        sample = 'fq';
    }

    if (options.sample === 'kinesis') {
        sample = 'kinesis';
    }

    const config: Config = {
        ...default_config,
        infile,
        outfile,
        input_format,
        output_format,
        sample,

        optimize_output
    };

    return config;
}

export function show_help(): void {
    console.log(
        commandLineUsage([
            {
                header: 'fq kinesis advantage keymap compiler',
                content:
                    'This tool converts a visual representation of a Kinesis Advantage \
keyboard layout into the format used by the keyboard itself. This visual representation is arguably easier to work with.'
            },
            {
                header: 'Options',
                optionList: options_def
            }
        ])
    );
}
