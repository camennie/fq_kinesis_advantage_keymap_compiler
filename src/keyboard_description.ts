/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { ActionToken } from './action_tokens/action_token';
import { KeyDescription } from './key_description';
import { KeyboardLayersDescription } from './keyboard_layers_description';
import { MultiKeyMacro } from './multi_key_macro';

export type KinesisKeyExprLookup = Map<string, KeyDescription>;

export class KeyboardDescription {
    private readonly desc_layers: KeyboardLayersDescription;
    private readonly lookup: KinesisKeyExprLookup;
    private readonly _macros: Map<string, string>;
    private readonly _multi_key_macros: MultiKeyMacro[];

    constructor(base: KeyboardLayersDescription) {
        this._macros = new Map();
        this._multi_key_macros = [];
        [this.desc_layers, this.lookup] = keyboard_description(base);
    }

    public replace_key_desc(kinesis_location: string, value: ActionToken): void {
        const key_desc = this.lookup.get(kinesis_location);

        if (key_desc !== undefined) {
            key_desc.value = value;
        } else {
            // tslint:disable-next-line:no-console
            console.error(`unable to set key at location: ${kinesis_location}`);
        }
    }

    public get desc(): KeyboardLayersDescription {
        return this.desc_layers;
    }

    public get multi_key_macros(): MultiKeyMacro[] {
        return this._multi_key_macros;
    }

    public get macros(): Map<string, string> {
        return this._macros;
    }

    public validate_kpshift_layers(): void {
        validate_kpshift_layers(this.desc_layers);
    }
}

function keyboard_description(
    base: KeyboardLayersDescription
): [KeyboardLayersDescription, KinesisKeyExprLookup] {
    const keyboard = base;
    const lookup = new Map();

    const layers = [keyboard.base, keyboard.keypad];
    for (const layer of layers) {
        add_lookups(lookup, [layer.left_function_row.keys]);
        add_lookups(lookup, layer.left_key_well.keys);
        add_lookups(lookup, layer.left_thumb_island.keys);

        add_lookups(lookup, [layer.right_function_row.keys]);
        add_lookups(lookup, layer.right_key_well.keys);
        add_lookups(lookup, layer.right_thumb_island.keys);

        add_lookups(lookup, [[layer.lp, layer.mp, layer.rp]]);
    }

    return [keyboard, lookup];
}

function add_lookups(lookup: KinesisKeyExprLookup, exprs: KeyDescription[][]): void {
    for (const row of exprs) {
        for (const expr of row) {
            if (lookup.has(expr.kinesis_location)) {
                // tslint:disable-next-line:no-console
                console.error(`!!! Duplicate base key: ${expr.kinesis_location}`);
            }

            lookup.set(expr.kinesis_location, expr);
        }
    }
}

function validate_kpshift_layers(base: KeyboardLayersDescription): void {
    const keyboard = base;
    const layers = [keyboard.base, keyboard.keypad];

    validate_kpshift_layer_keys(
        [layers[0].left_function_row.keys],
        [layers[1].left_function_row.keys]
    );
    validate_kpshift_layer_keys(layers[0].left_key_well.keys, layers[1].left_key_well.keys);
    validate_kpshift_layer_keys(layers[0].left_thumb_island.keys, layers[1].left_thumb_island.keys);

    validate_kpshift_layer_keys(
        [layers[0].right_function_row.keys],
        [layers[1].right_function_row.keys]
    );
    validate_kpshift_layer_keys(layers[0].right_key_well.keys, layers[1].right_key_well.keys);
    validate_kpshift_layer_keys(
        layers[0].right_thumb_island.keys,
        layers[1].right_thumb_island.keys
    );

    validate_kpshift_layer_keys(layers[0].left_thumb_island.keys, layers[1].left_thumb_island.keys);

    validate_kpshift_layer_keys(
        [[layers[0].lp, layers[0].mp, layers[0].rp]],
        [[layers[1].lp, layers[1].mp, layers[1].rp]]
    );
}

function validate_kpshift_layer_keys(keys_a: KeyDescription[][], keys_b: KeyDescription[][]): void {
    for (let row_i = 0; row_i < keys_a.length; row_i = row_i + 1) {
        const row_a = keys_a[row_i];
        const row_b = keys_b[row_i];

        for (let expr_i = 0; expr_i < row_a.length; expr_i = expr_i + 1) {
            const expr_a = row_a[expr_i];
            const expr_b = row_b[expr_i];

            const row_out_a = expr_a.value.write_kinesis(expr_a.kinesis_location);
            const row_out_b = expr_b.value.write_kinesis(expr_b.kinesis_location);

            const a_has_kpshift = row_out_a.includes('kpshift');
            const b_has_kpshift = row_out_b.includes('kpshift');

            if (a_has_kpshift !== b_has_kpshift) {
                console.error(`kpshift must be assigned to same key on both layers. \
There is a good chance the key at location ${expr_a.kinesis_location} with values \
${expr_a.value.write_fq()}/${expr_b.value.write_fq()} will not work correctly or kp mode will get stuck on.`);
            }
        }
    }
}
