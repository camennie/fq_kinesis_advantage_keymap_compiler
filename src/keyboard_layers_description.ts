/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { KeyDescription } from './key_description';

export interface LeftFunctionRow {
    keys: [
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription
    ];
}

export interface RightFunctionRow {
    keys: [
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription,
        KeyDescription
    ];
}

export interface KeyWell {
    keys: [
        [
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription
        ],
        [
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription
        ],
        [
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription
        ],
        [
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription,
            KeyDescription
        ],
        [KeyDescription, KeyDescription, KeyDescription, KeyDescription]
    ];
}

export interface ThumbIsland {
    keys: [
        [KeyDescription, KeyDescription],
        [KeyDescription, KeyDescription, KeyDescription],
        [KeyDescription]
    ];
}

export interface KeyboardLayer {
    left_function_row: LeftFunctionRow;
    left_key_well: KeyWell;
    left_thumb_island: ThumbIsland;

    right_function_row: RightFunctionRow;
    right_key_well: KeyWell;
    right_thumb_island: ThumbIsland;

    lp: KeyDescription;
    mp: KeyDescription;
    rp: KeyDescription;
}

export interface KeyboardLayersDescription {
    base: KeyboardLayer;
    keypad: KeyboardLayer;
}
