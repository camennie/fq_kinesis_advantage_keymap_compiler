/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { ActionToken } from './action_tokens/action_token';

export class MultiKeyMacro {
    private readonly token: ActionToken;
    private readonly triggers: string[];

    constructor(triggers: string[], token: ActionToken) {
        this.token = token;
        this.triggers = triggers;
    }

    public write_kinesis(): string {
        const triggers = this.triggers.map((t: string) => `{${t}}`).join('');
        const value = this.token.kinesis_macro_value();

        return `${triggers}>${value}`;
    }

    public write_fq(): string {
        const triggers = this.triggers.map((t: string) => `{${t}}`).join('');
        const value = this.token.write_fq();

        return `${triggers} > ${value}`;
    }
}
