/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { ActionToken } from './action_tokens/action_token';
import { ActionTokenKeyWithModifiers } from './action_tokens/action_token_key_with_modifiers';
import { ActionTokenMacro } from './action_tokens/action_token_macro';
import { ActionTokenModifier } from './action_tokens/action_token_modifier';
import { ActionTokenNull } from './action_tokens/action_token_null';
import { ActionTokenScancode } from './action_tokens/action_token_scancode';
import { ActionTokenShifted } from './action_tokens/action_token_shifted';
import { ActionTokenSimple } from './action_tokens/action_token_simple';
import { ActionTokenTapHold } from './action_tokens/action_token_taphold';
import { Config } from './config';
import { qwerty } from './default_keyboard_description_qwerty';
// import { null_keys } from './default_keyboard_description_null';
import { KeyDescription } from './key_description';
import { KeyboardDescription } from './keyboard_description';
import { zip } from 'lodash';
import { parse_location_tokens } from './load_kinesis_layout_file';
import { MultiKeyMacro } from './multi_key_macro';

// tslint:disable-next-line:max-func-body-length
export function load_fq_layout_file(base_lines: string[], _config: Config): KeyboardDescription {
    const lines = base_lines.filter(
        (l: string) => !l.trim().startsWith('#') && l.trim().length !== 0
    );

    const keyboard = new KeyboardDescription(qwerty);

    const group_lookup = init_group_lookup(keyboard);

    let cur_bucket: [string[], KeyDescription[][]] | undefined;

    for (const line of lines) {
        if (group_lookup.has(line.trim())) {
            cur_bucket = group_lookup.get(line.trim());
        } else {
            if (cur_bucket !== undefined) {
                cur_bucket[0].push(line.trim());
            } else {
                console.error(`Line outside of key group: ${line}`);
            }
        }
    }

    process_macros(keyboard.macros, group_lookup.get('[macros]'));

    const action_token_parsers = [
        new ActionTokenMacro(keyboard.macros, '', ''),
        new ActionTokenKeyWithModifiers(new Set(), ''),
        new ActionTokenNull(),
        new ActionTokenTapHold('', '', 0),
        new ActionTokenShifted('', ''),
        new ActionTokenScancode(''),
        new ActionTokenModifier('lshift'),
        new ActionTokenSimple('')
    ];

    for (const entry of group_lookup.entries()) {
        if (entry[0] === '[macros]' || entry[0] === '[multi key macros]') {
            continue;
        }

        process_group(action_token_parsers, entry);
    }

    keyboard.validate_kpshift_layers();

    process_multi_key_macros(
        action_token_parsers,
        keyboard.multi_key_macros,
        group_lookup.get('[multi key macros]')
    );

    return keyboard;
}

function init_group_lookup(
    keyboard: KeyboardDescription
): Map<string, [string[], KeyDescription[][]]> {
    const base_petals: KeyDescription[][] = [
        [keyboard.desc.base.lp, keyboard.desc.base.mp, keyboard.desc.base.rp]
    ];
    const kp_petals: KeyDescription[][] = [
        [keyboard.desc.keypad.lp, keyboard.desc.keypad.mp, keyboard.desc.keypad.rp]
    ];

    return new Map([
        ['[macros]', [[], []]],
        ['[multi key macros]', [[], []]],

        [
            '[base left function row]',
            [[], <KeyDescription[][]>[keyboard.desc.base.left_function_row.keys]]
        ],
        [
            '[base right function row]',
            [[], <KeyDescription[][]>[keyboard.desc.base.right_function_row.keys]]
        ],
        [
            '[base left key well rows]',
            [[], <KeyDescription[][]>keyboard.desc.base.left_key_well.keys]
        ],
        [
            '[base right key well rows]',
            [[], <KeyDescription[][]>keyboard.desc.base.right_key_well.keys]
        ],
        [
            '[base left thumb island rows]',
            [[], <KeyDescription[][]>keyboard.desc.base.left_thumb_island.keys]
        ],
        [
            '[base right thumb island rows]',
            [[], <KeyDescription[][]>keyboard.desc.base.right_thumb_island.keys]
        ],
        ['[base foot petal keys]', [[], base_petals]],

        [
            '[keypad left function row]',
            [[], <KeyDescription[][]>[keyboard.desc.keypad.left_function_row.keys]]
        ],
        [
            '[keypad right function row]',
            [[], <KeyDescription[][]>[keyboard.desc.keypad.right_function_row.keys]]
        ],
        [
            '[keypad left key well rows]',
            [[], <KeyDescription[][]>keyboard.desc.keypad.left_key_well.keys]
        ],
        [
            '[keypad right key well rows]',
            [[], <KeyDescription[][]>keyboard.desc.keypad.right_key_well.keys]
        ],
        [
            '[keypad left thumb island rows]',
            [[], <KeyDescription[][]>keyboard.desc.keypad.left_thumb_island.keys]
        ],
        [
            '[keypad right thumb island rows]',
            [[], <KeyDescription[][]>keyboard.desc.keypad.right_thumb_island.keys]
        ],
        ['[keypad foot petal keys]', [[], kp_petals]]
    ]);
}

function process_group(
    action_token_parsers: ActionToken[],
    entry: [string, [string[], KeyDescription[][]]]
): void {
    const group = entry[0];
    const [key_lines, keys] = entry[1];

    if (key_lines.length !== keys.length) {
        console.error(`Incorrect number of rows for group ${group}`);
    } else {
        const split_lines: string[][] = key_lines.map((l: string) =>
            l.split(' ').filter((v: string) => v.length > 0)
        );

        const zkl = zip(split_lines, keys);

        for (const kl of zkl) {
            if (kl[0] === undefined || kl[1] === undefined) {
                continue;
            }

            if (kl[0].length !== kl[1].length) {
                console.error(`Incorrect number of row key remaps for group ${group}`);
            }
        }

        for (const kl of zkl) {
            if (kl[0] === undefined || kl[1] === undefined) {
                continue;
            }

            const remap_keys: [string | undefined, KeyDescription | undefined][] = zip(
                kl[0],
                kl[1]
            );

            parse_replace_keys(action_token_parsers, remap_keys);
        }
    }
}

function process_macros(
    kb_macros: Map<string, string>,
    macros: [string[], KeyDescription[][]] | undefined
): void {
    if (macros === undefined) {
        return;
    }

    const r = new RegExp(/\<(\w+)\>\s*\<(.+)\>/);

    for (const macro of macros[0]) {
        const matched = r.exec(macro);

        if (matched !== null) {
            kb_macros.set(matched[1], matched[2]);
        } else {
            console.error(`Unknown macro entry: ${macro}`);
        }
    }
}

function process_multi_key_macros(
    action_token_parsers: ActionToken[],
    multi_key_macros: MultiKeyMacro[],
    macros: [string[], KeyDescription[][]] | undefined
): void {
    if (macros === undefined) {
        return;
    }

    for (const macro of macros[0]) {
        // noinspection DuplicatedCode
        const pieces = macro
            .trim()
            .split('>')
            .map((s: string) => s.trim());

        if (pieces.length !== 2) {
            console.error(`Unknown directive: ${macro}`);
            continue;
        }

        const locations = parse_location_tokens(pieces[0]);

        const action_token = pieces[1];
        let parsed = false;
        for (const op of action_token_parsers) {
            const token = op.parse_fq(action_token);

            if (token !== undefined && locations.length > 0) {
                multi_key_macros.push(new MultiKeyMacro(locations, token));

                parsed = true;
                break;
            }
        }

        if (!parsed) {
            console.error(`Unknown directive: ${macro}`);
        }
    }
}

function parse_replace_keys(
    action_token_parsers: ActionToken[],
    remap_keys: [string | undefined, KeyDescription | undefined][]
): void {
    for (const remap_key of remap_keys) {
        if (remap_key[0] === undefined || remap_key[1] === undefined) {
            continue;
        }

        if (!do_key_remap(action_token_parsers, remap_key[0], remap_key[1])) {
            console.error(`Unknown key remap: ${remap_key[0]}`);
        }
    }
}

function do_key_remap(
    action_token_parsers: ActionToken[],
    remap_key: string,
    key: KeyDescription
): boolean {
    for (const op of action_token_parsers) {
        const token = op.parse_fq(remap_key);

        if (token !== undefined) {
            key.value = token;

            return true;
        }
    }

    return false;
}
