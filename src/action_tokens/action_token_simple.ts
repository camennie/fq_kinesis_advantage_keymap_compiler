/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { is_valid_action_token } from '../kinesis_layout_key_defs';
import { unwrap } from '../misc';
import { ActionToken } from './action_token';

export class ActionTokenSimple implements ActionToken {
    private readonly val: string;

    constructor(val: string) {
        this.val = val;
    }

    public get value(): string {
        return this.val;
    }

    public parse_fq(v: string): ActionToken | undefined {
        const val = convert_from_shorthand(v);

        if (is_valid_action_token(val)) {
            return new ActionTokenSimple(val);
        } else {
            return undefined;
        }
    }

    public parse_kinesis(v: string): ActionToken | undefined {
        const unwrapped = unwrap(v);

        if (unwrapped !== undefined) {
            if (is_valid_action_token(unwrapped)) {
                return new ActionTokenSimple(unwrapped);
            } else {
                return undefined;
            }
        } else {
            return undefined;
        }
    }

    public write_fq(): string {
        const val = convert_to_shorthand(this.value);

        return val;
    }

    public write_kinesis(location_token: string): string {
        return `[${location_token}]>[${this.value}]`;
    }

    public kinesis_macro_value(): string {
        return `{${this.value}}`;
    }
}

const shorthands = new Map([['hyphen', '-'], ['obrack', '['], ['cbrack', ']']]);

const shorthands_rev = new Map<string, string>(
    [...shorthands.entries()].map(([k, v]: [string, string]) => [v, k])
);

function convert_to_shorthand(v: string): string {
    const s = shorthands.get(v);

    if (s === undefined) {
        return v;
    } else {
        return s;
    }
}

function convert_from_shorthand(v: string): string {
    const s = shorthands_rev.get(v);

    if (s === undefined) {
        return v;
    } else {
        return s;
    }
}
