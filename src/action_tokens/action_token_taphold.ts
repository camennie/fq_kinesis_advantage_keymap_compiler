/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { is_valid_action_token } from '../kinesis_layout_key_defs';
import { ActionToken } from './action_token';

const taphold_regex = new RegExp(/([^/]+)\/(\d+)\/([^/]+)/);
const kinesis_taphold_regex = new RegExp(/\[([^\]]+)\]\s*\[t&h(\d+)\]\[([^\]]+)\]/);

export class ActionTokenTapHold implements ActionToken {
    private readonly _tap: string;
    private readonly _hold: string;
    private readonly _delay: number;

    constructor(tap: string, hold: string, delay: number) {
        this._tap = tap;
        this._hold = hold;
        this._delay = delay;
    }

    public get tap(): string {
        return this._tap;
    }

    public get hold(): string {
        return this._hold;
    }

    public get delay(): number {
        return this._delay;
    }

    public parse_fq(v: string): ActionToken | undefined {
        return parse_common(taphold_regex, v);
    }

    public parse_kinesis(v: string): ActionToken | undefined {
        return parse_common(kinesis_taphold_regex, v);
    }

    public write_fq(): string {
        return `${this.tap}/${this.delay}/${this.hold}`;
    }

    public write_kinesis(location_token: string): string {
        return `[${location_token}]>[${this.tap}][t&h${this.delay}][${this.hold}]`;
    }

    public kinesis_macro_value(): string {
        console.error(
            `Tap and hold is not valid in a macro sequence. This action will not work: ${this.write_fq()}`
        );

        return `not implemented`;
    }
}

function parse_common(regex: RegExp, str: string): ActionTokenTapHold | undefined {
    const matched = regex.exec(str);

    if (matched !== null) {
        const tap = matched[1];
        const delay = parseInt(matched[2], 10);
        const hold = matched[3];

        if (is_valid_action_token(tap) && is_valid_action_token(hold)) {
            validate_tokens(tap, hold);

            return new ActionTokenTapHold(tap, hold, delay);
        } else {
            return undefined;
        }
    } else {
        return undefined;
    }
}

const bad_th_tokens = new Set<string>(['kpshift']);
function validate_tokens(tap: string, hold: string): void {
    if (bad_th_tokens.has(tap)) {
        console.error(`Tap and hold token ${tap} is known not to work!`);
    }

    if (bad_th_tokens.has(hold)) {
        console.error(`Tap and hold token ${hold} is known not to work!`);
    }
}
