/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import {
    get_shifted_key,
    get_unshifted_key,
    is_shifted_key,
    is_valid_action_token
} from '../kinesis_layout_key_defs';
import { ActionToken } from './action_token';
import {
    convert_modifier_token,
    is_default_modifier_key_locations,
    ModifierActionToken
} from './action_token_modifier';
import { ActionTokenShifted } from './action_token_shifted';

const modifiers_regex = new RegExp(
    /^([SCAW]*)\(([\d\w`=\\;',./\-+?<>":|_~!@#$%^&*()[\]{}]+)\)([SCAW]*)$/
);

export class ActionTokenKeyWithModifiers implements ActionToken {
    private readonly mods: Set<ModifierActionToken>;
    private readonly val: string;

    constructor(mods: Set<ModifierActionToken>, val: string) {
        this.val = val;
        this.mods = mods;
    }

    public get modifiers(): ModifierActionToken[] {
        return [...this.mods.values()];
    }

    public get value(): string {
        return this.val;
    }

    public parse_fq(v: string): ActionToken | undefined {
        const matched = modifiers_regex.exec(v);

        if (matched === null) {
            return undefined;
        }

        const left = matched[1].toUpperCase();
        let action = matched[2];
        const right = matched[3].toUpperCase();

        if (left.length === 0 && right.length === 0) {
            return undefined;
        }

        const mods: Set<ModifierActionToken> = new Set<ModifierActionToken>();

        if (left.includes('S')) {
            mods.add('lshift');
        }
        if (left.includes('C')) {
            mods.add('lctrl');
        }
        if (left.includes('A')) {
            mods.add('lalt');
        }
        if (left.includes('W')) {
            mods.add('lwin');
        }
        if (right.includes('S')) {
            mods.add('rshift');
        }
        if (right.includes('C')) {
            mods.add('rctrl');
        }
        if (right.includes('A')) {
            mods.add('ralt');
        }
        if (right.includes('W')) {
            mods.add('rwin');
        }

        if (is_shifted_key(action)) {
            const unshifted = get_unshifted_key(action);
            if (unshifted === undefined) {
                return undefined;
            } else {
                action = unshifted;
                mods.add('lshift');
            }
        }

        if (is_valid_action_token(action)) {
            return new ActionTokenKeyWithModifiers(mods, action);
        } else {
            return undefined;
        }
    }

    public parse_kinesis(v: string): ActionToken | undefined {
        type Mode = 'open' | 'close';
        let mode: Mode = 'open';
        const pieces = v.split('}').filter((s: string) => s.length > 0);
        const opens: string[] = [];
        const closes: string[] = [];
        let value: string = '';

        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < pieces.length; i = i + 1) {
            switch (mode) {
                case 'open':
                    if (pieces[i].startsWith('{-')) {
                        opens.push(pieces[i].substring(2));
                    } else {
                        if (pieces[i].startsWith('{')) {
                            value = pieces[i].substring(1);
                        } else {
                            return undefined;
                        }

                        mode = 'close';
                    }
                    break;

                case 'close':
                    if (pieces[i].startsWith('{+')) {
                        closes.push(pieces[i].substring(2));
                    } else {
                        return undefined;
                    }
                    break;

                default:
                    return undefined;
            }
        }

        if (!is_valid_action_token(value)) {
            return undefined;
        }

        opens.sort();
        closes.sort();

        const mods: Set<ModifierActionToken> = new Set<ModifierActionToken>();

        if (
            opens.length === closes.length &&
            opens.every((val: string, index: number) => val === closes[index])
        ) {
            for (const token of opens) {
                const mod_token = convert_modifier_token(token);

                if (mod_token !== undefined) {
                    mods.add(mod_token);
                } else {
                    return undefined;
                }
            }
        } else {
            return undefined;
        }

        if (mods.size === 1) {
            const mod = [...mods.values()][0];
            if (mod === 'lshift' || mod === 'rshift') {
                const shifted = get_shifted_key(value);
                if (shifted !== undefined) {
                    return new ActionTokenShifted(shifted, value);
                }
            }
        }

        return new ActionTokenKeyWithModifiers(mods, value);
    }

    public write_fq(): string {
        const lmodifiers: string[] = [];
        const rmodifiers: string[] = [];

        if (this.mods.has('lshift')) {
            lmodifiers.push('S');
        }

        if (this.mods.has('lctrl')) {
            lmodifiers.push('C');
        }

        if (this.mods.has('lalt')) {
            lmodifiers.push('A');
        }

        if (this.mods.has('lwin')) {
            lmodifiers.push('W');
        }

        if (this.mods.has('rshift')) {
            rmodifiers.push('S');
        }

        if (this.mods.has('rctrl')) {
            rmodifiers.push('C');
        }

        if (this.mods.has('ralt')) {
            rmodifiers.push('A');
        }

        if (this.mods.has('rwin')) {
            rmodifiers.push('W');
        }

        const prefix = `${lmodifiers.join('')}(`;
        const postfix = `)${rmodifiers.join('')}`;

        return prefix + this.value + postfix;
    }

    public write_kinesis(location_token: string): string {
        const value = this.kinesis_macro_value();

        if (is_default_modifier_key_locations(location_token)) {
            console.error(`Invalid location token ${location_token} for macro replacement with ${value} --\
Macros can't be triggered by a single key if that key is associated with one of the default modifier key locations`);
        }

        return `{${location_token}}>${value}`;
    }

    public kinesis_macro_value(): string {
        const mods = this.modifiers;
        const open_mods = mods.map((m: string) => `{-${m}}`).join('');
        const close_mods = mods.map((m: string) => `{+${m}}`).join('');

        const value = `${`${open_mods}{${this.value}}${close_mods}`}`;

        return value;
    }
}
