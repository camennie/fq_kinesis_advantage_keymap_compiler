/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { is_valid_action_token } from '../kinesis_layout_key_defs';
import { unwrap } from '../misc';
import { ActionToken } from './action_token';

const scancode_regex = new RegExp(/^{(\d+)}$/);
const kinesis_scancode_regex = new RegExp(/^(\d\d+)$/);

export class ActionTokenScancode implements ActionToken {
    private readonly val: string;

    constructor(val: string) {
        this.val = val;
    }

    public get value(): string {
        return this.val;
    }

    public parse_fq(v: string): ActionToken | undefined {
        const matched = scancode_regex.exec(v);

        if (matched !== null) {
            return new ActionTokenScancode(matched[1]);
        } else {
            return undefined;
        }
    }

    public parse_kinesis(v: string): ActionToken | undefined {
        const unwrapped = unwrap(v);

        if (unwrapped === undefined) {
            return undefined;
        }

        const matched = kinesis_scancode_regex.exec(unwrapped);

        if (matched !== null && is_valid_action_token(unwrapped)) {
            return new ActionTokenScancode(matched[1]);
        } else {
            return undefined;
        }
    }

    public write_fq(): string {
        return `{${this.value}}`;
    }

    public write_kinesis(location_token: string): string {
        return `[${location_token}]>[${this.value}]`;
    }

    public kinesis_macro_value(): string {
        return `{${this.value}}`;
    }
}
