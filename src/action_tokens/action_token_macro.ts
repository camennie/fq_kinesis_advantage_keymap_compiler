/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { ActionToken } from './action_token';
import { is_default_modifier_key_locations } from './action_token_modifier';

const macro_regex = new RegExp(/^\s*\${([\d\w_-]+)}\s*$/);
const kinesis_macro_regex = new RegExp(/^\s*{.+}\s*$/);

export class ActionTokenMacro implements ActionToken {
    private readonly _macros: Map<string, string>;
    private readonly _name: string;
    private readonly val: string;

    constructor(macros: Map<string, string>, name: string, val: string) {
        this._macros = macros;
        this._name = name;
        this.val = val;
    }

    public get value(): string {
        return this.val;
    }

    public get name(): string {
        return this._name;
    }

    public get macros(): Map<string, string> {
        return this._macros;
    }

    public parse_fq(v: string): ActionToken | undefined {
        const matched = macro_regex.exec(v);

        if (matched !== null) {
            const macro_name = matched[1];
            const macro_val = this.macros.get(macro_name);
            if (macro_val !== undefined) {
                return new ActionTokenMacro(this.macros, macro_name, macro_val);
            } else {
                return undefined;
            }
        } else {
            return undefined;
        }
    }

    public parse_kinesis(v: string): ActionToken | undefined {
        const matched = kinesis_macro_regex.exec(v);

        if (matched !== null) {
            const name = `M${this.macros.size + 1}`;
            this.macros.set(name, v);

            return new ActionTokenMacro(this.macros, name, v);
        } else {
            return undefined;
        }
    }

    public write_fq(): string {
        return `\${${this.name}}`;
    }

    public write_kinesis(location_token: string): string {
        const value = this.kinesis_macro_value();

        if (is_default_modifier_key_locations(location_token)) {
            console.error(`Invalid location token ${location_token} for macro replacement with ${value} --\
Macros can't be triggered by a single key if that key is associated with one of the default modifier key locations`);
        }

        return `{${location_token}}>${value}`;
    }

    public kinesis_macro_value(): string {
        return this.value;
    }
}
