/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { unwrap } from '../misc';
import { ActionToken } from './action_token';

export type ModifierActionToken =
    | 'lshift'
    | 'lctrl'
    | 'lalt'
    | 'lwin'
    | 'rshift'
    | 'rctrl'
    | 'ralt'
    | 'rwin';

export class ActionTokenModifier implements ActionToken {
    private readonly val: ModifierActionToken;

    constructor(val: ModifierActionToken) {
        this.val = val;
    }

    public get modifier(): ModifierActionToken {
        return this.val;
    }

    public parse_fq(v: string): ActionToken | undefined {
        const mod = convert_modifier_token(v);

        if (mod !== undefined) {
            return new ActionTokenModifier(mod);
        } else {
            return undefined;
        }
    }

    public parse_kinesis(v: string): ActionToken | undefined {
        const unwrapped = unwrap(v);

        if (unwrapped === undefined) {
            return undefined;
        }

        const mod = convert_modifier_token(unwrapped);

        if (unwrapped !== undefined && mod !== undefined) {
            return new ActionTokenModifier(mod);
        } else {
            return undefined;
        }
    }

    public write_fq(): string {
        return this.modifier;
    }

    public write_kinesis(location_token: string): string {
        return `[${location_token}]>[${this.modifier}]`;
    }

    public kinesis_macro_value(): string {
        return `{${this.modifier}}`;
    }
}

export function convert_modifier_token(v: string): ModifierActionToken | undefined {
    if (is_modifier_token(v)) {
        return v as ModifierActionToken;
    } else {
        return undefined;
    }
}

function is_modifier_token(v: string): boolean {
    switch (v) {
        case 'lshift':
        case 'lctrl':
        case 'lalt':
        case 'lwin':
        case 'rshift':
        case 'rctrl':
        case 'ralt':
        case 'rwin':
            return true;

        default:
            return false;
    }
}

export function is_default_modifier_key_locations(v: string): boolean {
    switch (v) {
        case 'lshift':
        case 'lctrl':
        case 'lalt':
        case 'rshift':
        case 'rctrl':
        case 'rwin':
        case 'kp-lshift':
        case 'kp-lctrl':
        case 'kp-lalt':
        case 'kp-rshift':
        case 'kp-rctrl':
        case 'kp-rwin':
            return true;

        default:
            return false;
    }
}
