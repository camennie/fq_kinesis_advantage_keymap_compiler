/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { ActionToken } from './action_token';

export class ActionTokenNull implements ActionToken {
    public parse_fq(v: string): ActionToken | undefined {
        if (v === 'null') {
            return this;
        } else {
            return undefined;
        }
    }

    public parse_kinesis(v: string): ActionToken | undefined {
        if (v === '[null]') {
            return this;
        } else {
            return undefined;
        }
    }

    public write_fq(): string {
        return 'null';
    }

    public write_kinesis(location_token: string): string {
        return `[${location_token}]>[null]`;
    }

    public kinesis_macro_value(): string {
        return `{${null}}`;
    }
}
