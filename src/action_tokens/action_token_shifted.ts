/*
 * Copyright (c) 2019 Chris Mennie
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>
 */

import { get_unshifted_key, is_shifted_key } from '../kinesis_layout_key_defs';
import { ActionToken } from './action_token';
import { is_default_modifier_key_locations } from './action_token_modifier';

export class ActionTokenShifted implements ActionToken {
    private readonly sh: string;
    private readonly unsh: string;

    constructor(shifted: string, unshifted: string) {
        this.sh = shifted;
        this.unsh = unshifted;
    }

    public get shifted(): string {
        return this.sh;
    }

    public get unshifted(): string {
        return this.unsh;
    }

    public parse_fq(v: string): ActionToken | undefined {
        if (is_shifted_key(v)) {
            const unshifted = get_unshifted_key(v);

            if (unshifted !== undefined) {
                return new ActionTokenShifted(v, unshifted);
            } else {
                return undefined;
            }
        } else {
            return undefined;
        }
    }

    public parse_kinesis(_v: string): ActionToken | undefined {
        return undefined;
    }

    public write_fq(): string {
        return this.shifted;
    }

    public write_kinesis(location_token: string): string {
        const value = this.kinesis_macro_value();

        if (is_default_modifier_key_locations(location_token)) {
            console.error(`Invalid location token ${location_token} for macro replacement with ${value} --\
Macros can't be triggered by a single key if that key is associated with one of the default modifier key locations`);
        }

        return `{${location_token}}>${value}`;
    }

    public kinesis_macro_value(): string {
        const value = `{-lshift}{${this.unshifted}}{+lshift}`;

        return value;
    }
}
