# Introduction
This tool converts a visual representation of a Kinesis Advantage 2
keyboard layout into the format used by the keyboard itself. This
visual representation is arguably easier to work with.

# Building
npm run build && npm run dist

# Usage
From lib folder:

ex: node dist/fq_advantage_layout.js --infile qwerty_fq.txt --outfile 1_qwerty

````
node index.ts [--infile <filename>] [--outfile <filename>] 
              [--informat <fq|kinesis>] [--outformat <fq|kinesis>] 
              [--sample <fq|kinesis>]
````

* --infile <filename\> : filename to read layout from \(stdin if not specified\) 
* --outfile <filename\> : filename to write layout to \(stdout if not specified\)
* --informat <fq|kinesis\> : force parsing format of input file \(autodetect if not specified\) 
* --outformat <fq|kinesis\> : layout of the output file as either a Kinesis layout or fq layout
 \(otherwise use opposite of input format if not specified\)
* --sample <fq|kinesis\> : Instead of processing an input file, write out an example layout in the 
format specified

Copy output file into "active" folder on keyboard.

# Layout
Example layouts can be found in the examples folder.

The fq layout is broken into a section for each major area on the
keyboard and foot petals, for both the base and keypad layers.
Each key is represented by an entry at the respective row and column.

For example:


```
[base right function row]
#
f9        f10       f11       f12       prtscr    scroll    pause
#
[base left key well rows]
#
=         1         2         3         4         5
tab       q         h         o         u         x
lctrl     y         i         e         a         .
lshift    j         /         ,         k         '
left      down      up        right
```

### Layout values
There are a few types of key values:

* comments : Any line beginning with a # character is considered to be commented.

* basic : These are the same as the Kinesis layout action tokens. However,
case does matter.

* scancodes : USB HID scancodes can be represented by \{<scancode\>\}. 
For example:
    * \{088\}
    * \{88\}    
    
* shifted : Single shifted letters or punctuation \(ex: AEI@\#$\>"\) will be converted 
into equivalent macros in the Kinesis layout.

* modifiers : Single action tokens with modifiers applied \(ctrl, shift, etc\).
We represent this as <left modifiers\>\(<token\>\)<right modifiers\> 
where modifiers are C for ctrl, S for shift, A for alt, W for super. 
For example:
    * C\(a\)  
    * CA\(delete\)  
    * SA\(f8\)  
    * \(c\)SC  
    * \(insert\)S
    
* tap and hold : Tap and hold actions are declared as 
<tap action token\>/<delay value\>/<modify action token\>. The tap and hold action tokens 
are any valid Kinesis layout action token. The delay is a number in milliseconds.
Note the lack of spaces between the separator slashes. 
For example:
    * escape/150/lctrl
    
* macros : Macros are declared in a separate section of the layout.
Each macro is given a name, which is used later in the key layout section. 
Macro names can contain letters, numbers, underscores, or hyphens.
To reference a macro, the format $\{<name\>\} is used. For example: 
    * $\{M-1\}
    
* multi key macros : To declare a macro that's triggered by multiple key presses (ie: rctrl and vol-)
we add another section of the layout file, "\[multi key macros\]". Each entry starts with the
location tokens, like you'd enter in a Kinesis layout file, followed by '>', then the value 
to expand out. This value can be any valid layout value, including macro values. 
For example:
    * {rctrl}{vol-} > $\{M1\}
    * {lshift}{q} > C\(w\) 
 

# Notes
The Kinesis layout output is based on qwerty so files should be named
<key\>_qwerty.txt before copying to keyboard.

If the input is a Kinesis layout, the tool will try to convert entries
into the more ergonomic fq representation where appropriate.

All the same restrictions to a regular Kinesis layout apply to the fq layout \(such
as kpshift needing to be on both layers of the same key\).



